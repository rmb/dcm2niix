dcm2niix (1.0.20211006-3) UNRELEASED; urgency=medium

  * Team upload.
  * Add autopkgtests
  * Install examples
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * add example data tarball to d/s/include-binaries
  * Remove test data & use multi-orig tarball instead
  * Add script to fetch test data
  * New upstream version 1.0.20211006
  * d/t/: Update tests
  * Add copyright for test data

 -- Mohammed Bilal <mdbilal@disroot.org>  Mon, 23 May 2022 15:23:34 +0530

dcm2niix (1.0.20211006-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Team upload.
  * Fix FTCBFS: Annotate python3-sphinx build dependency with :native.
    (Closes: #1010532)

 -- Andreas Tille <tille@debian.org>  Tue, 03 May 2022 21:28:51 +0200

dcm2niix (1.0.20211006-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.0.20211006
  * Update copyright metadata
  * Promote pigz to Recommends
  * Raise standards version to 4.6.0

 -- Ghislain Vaillant <ghisvail@gmail.com>  Sun, 10 Oct 2021 21:33:59 +0200

dcm2niix (1.0.20210317-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 18:16:00 +0530

dcm2niix (1.0.20201102-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 13 Nov 2020 13:38:20 +0530

dcm2niix (1.0.20200331-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 29 Sep 2020 18:05:29 +0530

dcm2niix (1.0.20181125-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Wed, 30 Jan 2019 10:37:36 +0100

dcm2niix (1.0.20180622-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 18 Sep 2018 14:33:27 +0200

dcm2niix (1.0.20171215-1) unstable; urgency=medium

  * Ensure tags are signed
  * New upstream version 1.0.20171215
  * Bump the debhelper version to 11
  * Bump the standards version to 4.1.3
  * Drop get-orig-source target, use uscan instead

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sun, 04 Feb 2018 14:21:38 +0000

dcm2niix (1.0.20171017-1) unstable; urgency=medium

  * New upstream version 1.0.20171017
  * Install the upstream change log

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 21 Oct 2017 22:44:36 +0100

dcm2niix (1.0.20170923-1) unstable; urgency=medium

  * New upstream version 1.0.20170923
  * Bump the standards version to 4.1.1

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 09 Oct 2017 18:45:04 +0100

dcm2niix (1.0.20170818-1) unstable; urgency=medium

  * New upstream version 1.0.20170818
  * Add pigz to Suggests
  * Bump standards version to 4.1.0

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 23 Aug 2017 19:35:10 +0100

dcm2niix (1.0.20170724-1) unstable; urgency=medium

  * Upgrade watch file to version 4
  * New upstream version 1.0.20170724
  * Bump standards version to 4.0.1, no changes required
  * Add missing get-orig-source target
  * Build with JPEG support using TurboJPEG

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 12 Aug 2017 19:03:13 +0100

dcm2niix (1.0.20170624-1) unstable; urgency=medium

  * New upstream version 1.0.20170624
  * Bump standards version to 4.0.0, no changes required

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 13 Jul 2017 19:03:13 +0100

dcm2niix (1.0.20170528-1) experimental; urgency=medium

  * New upstream version 1.0.20170528

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 03 Jun 2017 09:06:53 +0100

dcm2niix (1.0.20170429-1) experimental; urgency=medium

  * New upstream version 1.0.20170429
  * Update copyright information
  * Drop the patch queue, no longer required
  * Specify the source directory and build system
  * Use the system zlib and openjpeg2 libraries
  * Build and install the manpages with Sphinx

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 11 May 2017 18:46:40 +0100

dcm2niix (1.0.20161101-1) unstable; urgency=low

  * Initial release. (Closes: #843799)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 01 Dec 2016 18:42:22 +0000
