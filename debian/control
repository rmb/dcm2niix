Source: dcm2niix
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               libopenjp2-7-dev,
               libturbojpeg0-dev,
               libyaml-cpp-dev,
               pkg-config,
               python3-sphinx:native,
               zlib1g-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/dcm2niix
Vcs-Git: https://salsa.debian.org/med-team/dcm2niix.git
Homepage: https://github.com/rordenlab/dcm2niix
Rules-Requires-Root: no

Package: dcm2niix
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: pigz
Description: next generation DICOM to NIfTI converter
 dcm2niix is the successor of dcm2nii, a popular tool for converting images
 from the complicated formats used by scanner manufacturers (DICOM, PAR/REC)
 to the simpler NIfTI format used by many scientific tools. It works for all
 modalities (CT, MRI, PET, SPECT) and sequence types.
